#include <stdio.h>

typedef struct data {
    int line;
    int num;
    int datum;
    struct data *next;
} Data;

// Global Variable
Data *data;
int data_line;

void appendDatum(int l, int n, int d);
int lookupData(int l);
void toData(FILE *data);
void printData(void);
