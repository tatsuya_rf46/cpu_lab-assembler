#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "label.h"
#include "instruction.h"

void setGlobal(char* g) {
    globl = malloc(sizeof(char)*strlen(g));
    strcpy(globl, g);
}

Bundle *createBundle(
        int line,
        Instruction i0,
        Instruction i1,
        Instruction i2,
        int type
        ) {
    Bundle *ret = malloc(sizeof(Bundle));
    ret->line = line;
    ret->i0 = i0;
    ret->i1 = i1;
    ret->i2 = i2;
    ret->type = type;
    ret->next = NULL;

    return ret;
}

void appendBundle(
        int line,
        Instruction i0,
        Instruction i1,
        Instruction i2,
        int type
        ) {
    Bundle *new_bundle = createBundle(line, i0, i1, i2, type);
    if(bundles == NULL) bundles = new_bundle;
    else {
        Bundle *b = bundles;
        for (; b->next != NULL; b = b->next);
        b->next = new_bundle;
    }
}

Instruction *returnInstruction(
        enum mnemonic mnemo,
        enum reg source,
        enum reg target,
        enum reg dist,
        int immi,
        char *label
        ) {
    Instruction *ret = malloc(sizeof(Instruction));
    ret->mnemo = mnemo;
    ret->source= source;
    ret->target = target;
    ret->dist = dist;
    ret->immi = immi;
    ret->label = label;
    ret->label_line = -1;

    return ret;
}
// Instruction *createInstruction(
//         int line,
//         enum mnemonic mnemo,
//         enum reg source,
//         enum reg target,
//         enum reg dist,
//         int immi,
//         char *label
//         ) {
//     Instruction *ret = malloc(sizeof(Instruction));
//     ret->line = line;
//     ret->mnemo = mnemo;
//     ret->source= source;
//     ret->target = target;
//     ret->dist = dist;
//     ret->immi = immi;
//     ret->label = label;
//     ret->label_line = -1;
//     ret->next = NULL;

//     return ret;
// }

// void appendInstruction(
//         int line,
//         enum mnemonic mnemo,
//         enum reg source,
//         enum reg target,
//         enum reg dist,
//         int immi,
//         char *label
//         ) {
//     Instruction *new_instr =createInstruction(
//             line, mnemo, source, target, dist, immi, label
//             );

//     if (instrs == NULL) instrs = new_instr;
//     else {
//         Instruction *i = instrs;
//         for (; i->next != NULL; i = i->next);
//         i->next = new_instr;
//     }
// }

void incrementLineOfBundle(Bundle *bundle) {
    for(Bundle *b = bundle; b != NULL; b = b->next) b->line += 4;
}
// void incrementLineOfInstructions(Instruction *i) {
//     for (Instruction *j = i; j != NULL; j = j->next) j->line += 4;
// }

void incrementLineOfLabels(int line) {
    for (Label *l = labels; l != NULL; l = l->next) {
        if (l->line > line) l->line += 4;
    }
}

void replaceLI(Instruction *i) {
    if (i->immi >= (1 << 15)) {
        perror("LI's immidiately is too big");
        exit(1);
    }

    i->mnemo = ADDI;
    i->source = ZERO;
    i->target = i->dist;
    i->dist = None;
}
void replaceLIs(Bundle *b) {
    if (b->i0.mnemo == LI) replaceLI(&(b->i0));
    if (b->i1.mnemo == LI) replaceLI(&(b->i1));
    if (b->i2.mnemo == LI) replaceLI(&(b->i2));
}

void replaceMOVE(Instruction *i) {
    i->mnemo = ADD;
    i->target = i->source;
    i->source = ZERO;
    i->dist = i->dist;
}
void replaceMOVEs(Bundle *b) {
    if (b->i0.mnemo == MOVE) replaceMOVE(&(b->i0));
    if (b->i1.mnemo == MOVE) replaceMOVE(&(b->i1));
    if (b->i2.mnemo == MOVE) replaceMOVE(&(b->i2));
}

void replacePseudoinstructions(void) {
    if (bundles == NULL) return;

    for (Bundle *b = bundles; b->next != NULL; b = b->next) {
        replaceLIs(b);
        replaceMOVEs(b);
    }
    return;
}

void attachLabelLine(void) {
    for (Bundle *b = bundles; b != NULL; b = b->next) {
        if (b->i0.label != NULL) (&(b->i0))->label_line = lookupLabel(b->i0.label);
        if (b->i1.label != NULL) (&(b->i1))->label_line = lookupLabel(b->i1.label);
        if (b->i2.label != NULL) (&(b->i2))->label_line = lookupLabel(b->i2.label);
    }
    return;
}

char *itob(int num, char *buf, int d) {
    for (int i=0; i < d; i++) buf[i] = (num >> (d-i-1) & 1) ? '1' : '0';
    return buf;
}

void freg(enum reg reg, FILE *f) {
    char buf[8];
    itob(reg, buf, 8);
    fwrite(buf, 8, sizeof(char), f);
}

void fshamt(int shamt, FILE *f) {
    char buf[5];
    if (shamt >= 0b1 << 5) {
        perror("shamt is too big");
        exit(1);
    } else {
        itob(shamt, buf, 5);
        fwrite(buf, 5, sizeof(char), f);
    }
}

void fimmi(int immi, FILE *f) {
    char buf[19];
    itob(immi, buf, 19);
    fwrite(buf, 19, sizeof(char), f);
}

void faddr(int addr, FILE *f) {
    char buf[32];
    itob(addr, buf, 32);
    fwrite("000", 3, sizeof(char), f);
    fwrite(buf, 32, sizeof(char), f);
}

void fcc(int cc, FILE *f) {
    char buf[3];
    if (cc >= 0b1 << 3) {
        printf("line: %d\n", line);
        perror("cc is too big");
        exit(1);
    } else {
    itob(cc, buf, 3);
    fwrite(buf, 3, sizeof(char), f);
    }
}

void fline(int line, FILE *f) {
    fprintf(f, "%4x: ", line);
}

void flabel(int line, int maxlen, FILE *f) {
    Label *l;
    for (l = labels; l != NULL; l = l->next)
        if (l->line == line) break;

    int len = 0;

    if (l != NULL)
        fprintf(f, "%s%n", l->name, &len);

    for (int i=0; i<maxlen-len; i++)
        fwrite(" ", 1, sizeof(char), f);

    fwrite(": ", 2, sizeof(char), f);
}
void fnonlabel(int line, int maxlen, FILE *f) {
    for (int i=0; i<maxlen; i++) fwrite(" ", 1, sizeof(char), f);
    fwrite(": ", 2, sizeof(char), f);
}

void fregname(enum reg reg, FILE *f) {
    switch (reg) {
        case ZERO: fwrite("$zero", 5, sizeof(char), f); break;
        case V0: fwrite("$v0", 3, sizeof(char), f); break;
        case V1: fwrite("$v1", 3, sizeof(char), f); break;
        case AT: fwrite("$at", 3, sizeof(char), f); break;
        case A0: fwrite("$a0", 3, sizeof(char), f); break;
        case A1: fwrite("$a1", 3, sizeof(char), f); break;
        case A2: fwrite("$a2", 3, sizeof(char), f); break;
        case A3: fwrite("$a3", 3, sizeof(char), f); break;
        case T0: fwrite("$t0", 3, sizeof(char), f); break;
        case T1: fwrite("$t1", 3, sizeof(char), f); break;
        case T2: fwrite("$t2", 3, sizeof(char), f); break;
        case T3: fwrite("$t3", 3, sizeof(char), f); break;
        case T4: fwrite("$t4", 3, sizeof(char), f); break;
        case T5: fwrite("$t5", 3, sizeof(char), f); break;
        case T6: fwrite("$t6", 3, sizeof(char), f); break;
        case T7: fwrite("$t7", 3, sizeof(char), f); break;
        case S0: fwrite("$s0", 3, sizeof(char), f); break;
        case S1: fwrite("$s1", 3, sizeof(char), f); break;
        case S2: fwrite("$s2", 3, sizeof(char), f); break;
        case S3: fwrite("$s3", 3, sizeof(char), f); break;
        case S4: fwrite("$s4", 3, sizeof(char), f); break;
        case S5: fwrite("$s5", 3, sizeof(char), f); break;
        case S6: fwrite("$s6", 3, sizeof(char), f); break;
        case S7: fwrite("$s7", 3, sizeof(char), f); break;
        case T8: fwrite("$t8", 3, sizeof(char), f); break;
        case T9: fwrite("$t9", 3, sizeof(char), f); break;
        case K0: fwrite("$k0", 3, sizeof(char), f); break;
        case K1: fwrite("$k1", 3, sizeof(char), f); break;
        case GP: fwrite("$gp", 3, sizeof(char), f); break;
        case SP: fwrite("$sp", 3, sizeof(char), f); break;
        case FP: fwrite("$fp", 3, sizeof(char), f); break;
        case RA: fwrite("$ra", 3, sizeof(char), f); break;
        case None: fwrite("None", 5, sizeof(char), f); break;
    }
}

void ffregname(enum reg reg, FILE *f) {
    switch (reg) {
        case ZERO: fwrite("$f0", 3, sizeof(char), f); break;
        case AT: fwrite("$f1", 3, sizeof(char), f); break;
        case V0: fwrite("$f2", 3, sizeof(char), f); break;
        case V1: fwrite("$f3", 3, sizeof(char), f); break;
        case A0: fwrite("$f4", 3, sizeof(char), f); break;
        case A1: fwrite("$f5", 3, sizeof(char), f); break;
        case A2: fwrite("$f6", 3, sizeof(char), f); break;
        case A3: fwrite("$f7", 3, sizeof(char), f); break;
        case T0: fwrite("$f8", 3, sizeof(char), f); break;
        case T1: fwrite("$f9", 3, sizeof(char), f); break;
        case T2: fwrite("$f10", 3, sizeof(char), f); break;
        case T3: fwrite("$f11", 4, sizeof(char), f); break;
        case T4: fwrite("$f12", 4, sizeof(char), f); break;
        case T5: fwrite("$f13", 4, sizeof(char), f); break;
        case T6: fwrite("$f14", 4, sizeof(char), f); break;
        case T7: fwrite("$f15", 4, sizeof(char), f); break;
        case S0: fwrite("$f16", 4, sizeof(char), f); break;
        case S1: fwrite("$f17", 4, sizeof(char), f); break;
        case S2: fwrite("$f18", 4, sizeof(char), f); break;
        case S3: fwrite("$f19", 4, sizeof(char), f); break;
        case S4: fwrite("$f20", 4, sizeof(char), f); break;
        case S5: fwrite("$f21", 4, sizeof(char), f); break;
        case S6: fwrite("$f22", 4, sizeof(char), f); break;
        case S7: fwrite("$f23", 4, sizeof(char), f); break;
        case T8: fwrite("$f24", 4, sizeof(char), f); break;
        case T9: fwrite("$f25", 4, sizeof(char), f); break;
        case K0: fwrite("$f26", 4, sizeof(char), f); break;
        case K1: fwrite("$f27", 4, sizeof(char), f); break;
        case GP: fwrite("$f28", 4, sizeof(char), f); break;
        case SP: fwrite("$f29", 4, sizeof(char), f); break;
        case FP: fwrite("$f30", 4, sizeof(char), f); break;
        case RA: fwrite("$f31", 4, sizeof(char), f); break;
        case None: fwrite("None", 5, sizeof(char), f); break;
    }
}

void instructionToBinary(FILE *output, int line, Instruction *i) {
    switch (i->mnemo) {
        case NOP:
            fwrite("111011", 6, sizeof(char), output);
            fwrite("00000000000000000000000000000000000", 35, sizeof(char), output);
            break;
        // R-type
        case ADD:
            fwrite("000000", 6, sizeof(char), output);
            freg(i->source, output);
            freg(i->target, output);
            freg(i->dist, output);
            fwrite("00000", 5, sizeof(char), output);
            fwrite("100000", 6, sizeof(char), output);
            break;
        case AND:
            fwrite("000000", 6, sizeof(char), output);
            freg(i->source, output);
            freg(i->target, output);
            freg(i->dist, output);
            fwrite("00000", 5, sizeof(char), output);
            fwrite("100100", 6, sizeof(char), output);
            break;
        case JR:
            fwrite("000000", 6, sizeof(char), output);
            freg(i->source, output);
            freg(ZERO, output);
            freg(i->source, output);
            fwrite("00000", 5, sizeof(char), output);
            fwrite("001000", 6, sizeof(char), output);
            break;
        case JALR:
            fwrite("000000", 6, sizeof(char), output);
            freg(i->source, output);
            freg(ZERO, output);
            freg(i->source, output);
            fwrite("00000", 5, sizeof(char), output);
            fwrite("001001", 6, sizeof(char), output);
            break;
        case OR:
            fwrite("000000", 6, sizeof(char), output);
            freg(i->source, output);
            freg(i->target, output);
            freg(i->dist, output);
            fwrite("00000", 5, sizeof(char), output);
            fwrite("100101", 6, sizeof(char), output);
            break;
        case SLL:
            fwrite("000000", 6, sizeof(char), output);
            freg(ZERO, output);
            freg(i->target, output);
            freg(i->dist, output);
            fshamt(i->immi, output);
            fwrite("000000", 6, sizeof(char), output);
            break;
        case SRL:
            fwrite("000000", 6, sizeof(char), output);
            freg(ZERO, output);
            freg(i->target, output);
            freg(i->dist, output);
            fshamt(i->immi, output);
            fwrite("000010", 6, sizeof(char), output);
            break;
        case SLT:
            fwrite("000000", 6, sizeof(char), output);
            freg(i->source, output);
            freg(i->target, output);
            freg(i->dist, output);
            fwrite("00000", 5, sizeof(char), output);
            fwrite("101010", 6, sizeof(char), output);
            break;
        case SUB:
            fwrite("000000", 6, sizeof(char), output);
            freg(i->source, output);
            freg(i->target, output);
            freg(i->dist, output);
            fwrite("00000", 5, sizeof(char), output);
            fwrite("100010", 6, sizeof(char), output);
            break;
        // I-type
        case ADDI:
            fwrite("001000", 6, sizeof(char), output);
            freg(i->source, output);
            freg(i->target, output);
            fimmi(i->immi, output);
            break;
        case ORI:
            fwrite("001101", 6, sizeof(char), output);
            freg(i->source, output);
            freg(i->target, output);
            fimmi(i->immi, output);
            break;
        case SLTI:
            fwrite("001010", 6, sizeof(char), output);
            freg(i->source, output);
            freg(i->target, output);
            fimmi(i->immi, output);
            break;
        case BEQ:
            fwrite("000100", 6, sizeof(char), output);
            freg(i->source, output);
            freg(i->target, output);
            fimmi((i->label_line - line - 4) >> 2, output);
            break;
        case BNE:
            fwrite("000101", 6, sizeof(char), output);
            freg(i->source, output);
            freg(i->target, output);
            fimmi((i->label_line - line - 4) >> 2, output);
            break;
        case LUI:
            fwrite("001111", 6, sizeof(char), output);
            freg(ZERO, output);
            freg(i->target, output);
            fimmi(i->immi, output);
            break;
        case LW:
            fwrite("100011", 6, sizeof(char), output);
            freg(i->source, output);
            freg(i->target, output);
            fimmi(i->immi, output);
            break;
        case SW:
            fwrite("101011", 6, sizeof(char), output);
            freg(i->source, output);
            freg(i->target, output);
            fimmi(i->immi, output);
            break;
        // J-type
        case J:
            fwrite("000010", 6, sizeof(char), output);
            faddr(i->label_line >> 2, output);
            break;
        case JAL:
            fwrite("000011", 6, sizeof(char), output);
            faddr(i->label_line >> 2, output);
            break;
        // IO Instructions
        case IN:
            fwrite("011010", 6, sizeof(char), output);
            freg(ZERO, output);
            freg(i->target, output);
            fwrite("0000000000000000000", 19, sizeof(char), output);
            break;
        case OUT:
            fwrite("011011", 6, sizeof(char), output);
            freg(ZERO, output);
            freg(i->target, output);
            fwrite("0000000000000000000", 19, sizeof(char), output);
            break;
        // Float Instructions
        case BTS:
            fwrite("010001", 6, sizeof(char), output);
            fwrite("01000000", 8, sizeof(char), output);
            fcc(i->immi, output);
            fwrite("01000", 5, sizeof(char), output);
            fimmi((i->label_line - line - 4) >> 2, output);
            break;
        case BFS:
            fwrite("010001", 6, sizeof(char), output);
            fwrite("01000000", 8, sizeof(char), output);
            fcc(i->immi, output);
            fwrite("00000", 5, sizeof(char), output);
            fimmi((i->label_line - line - 4) >> 2, output);
            break;
        case ADDS:
            fwrite("010001", 6, sizeof(char), output);
            fwrite("10000000", 8, sizeof(char), output);
            freg(i->target, output);
            freg(i->source, output);
            freg(i->dist, output);
            fwrite("000", 3, sizeof(char), output);
            break;
        case SUBS:
            fwrite("010001", 6, sizeof(char), output);
            fwrite("10000000", 8, sizeof(char), output);
            freg(i->target, output);
            freg(i->source, output);
            freg(i->dist, output);
            fwrite("001", 3, sizeof(char), output);
            break;
        case MULS:
            fwrite("010001", 6, sizeof(char), output);
            fwrite("10000000", 8, sizeof(char), output);
            freg(i->target, output);
            freg(i->source, output);
            freg(i->dist, output);
            fwrite("010", 3, sizeof(char), output);
            break;
        case DIVS:
            fwrite("010001", 6, sizeof(char), output);
            fwrite("10000000", 8, sizeof(char), output);
            freg(i->target, output);
            freg(i->source, output);
            freg(i->dist, output);
            fwrite("011", 3, sizeof(char), output);
            break;
        case MOVS:
            fwrite("010001", 6, sizeof(char), output);
            fwrite("10000000", 8, sizeof(char), output);
            freg(i->target, output);
            freg(ZERO, output);
            freg(i->dist, output);
            fwrite("110", 3, sizeof(char), output);
            break;
        case NEGS:
            fwrite("010001", 6, sizeof(char), output);
            fwrite("10000000", 8, sizeof(char), output);
            freg(i->target, output);
            freg(ZERO, output);
            freg(i->dist, output);
            fwrite("111", 3, sizeof(char), output);
            break;
        case ABSS:
            fwrite("010001", 6, sizeof(char), output);
            fwrite("10000000", 8, sizeof(char), output);
            freg(i->target, output);
            freg(ZERO, output);
            freg(i->dist, output);
            fwrite("101", 3, sizeof(char), output);
            break;
        case SQRTS:
            fwrite("010001", 6, sizeof(char), output);
            fwrite("10000000", 8, sizeof(char), output);
            freg(i->target, output);
            freg(ZERO, output);
            freg(i->dist, output);
            fwrite("100", 3, sizeof(char), output);
            break;
        case CEQS:
            fwrite("010001", 6, sizeof(char), output);
            fwrite("00000000", 8, sizeof(char), output);
            freg(i->target, output);
            freg(i->source, output);
            fcc(i->immi, output);
            fwrite("00110010", 8, sizeof(char), output);
            break;
        case CLTS:
            fwrite("010001", 6, sizeof(char), output);
            fwrite("00000000", 8, sizeof(char), output);
            freg(i->target, output);
            freg(i->source, output);
            fcc(i->immi, output);
            fwrite("00111100", 8, sizeof(char), output);
            break;
        case CLES:
            fwrite("010001", 6, sizeof(char), output);
            fwrite("00000000", 8, sizeof(char), output);
            freg(i->target, output);
            freg(i->source, output);
            fcc(i->immi, output);
            fwrite("00111110", 8, sizeof(char), output);
            break;
        case LWS:
            fwrite("110001", 6, sizeof(char), output);
            freg(i->source, output);
            freg(i->target, output);
            fimmi(i->immi, output);
            break;
        case SWS:
            fwrite("111001", 6, sizeof(char), output);
            freg(i->source, output);
            freg(i->target, output);
            fimmi(i->immi, output);
            break;
        case FTOI:
            fwrite("111000", 6, sizeof(char), output);
            freg(i->source, output);
            freg(i->target, output);
            fwrite("0000000000000000000", 19, sizeof(char), output);
            break;
        case ITOF:
            fwrite("110000", 6, sizeof(char), output);
            freg(i->source, output);
            freg(i->target, output);
            fwrite("0000000000000000000", 19, sizeof(char), output);
            break;
        // Parallelization instructions
        case CADD:
            fwrite("111100", 6, sizeof(char), output);
            freg(i->source, output);
            freg(i->target, output);
            fwrite("0000000000000000000", 19, sizeof(char), output);
            break;
        case FORK:
            fwrite("111101", 6, sizeof(char), output);
            freg(i->source, output);
            freg(ZERO, output);
            fimmi(i->immi, output);
            break;
        case NEXT:
            fwrite("111110", 6, sizeof(char), output);
            freg(ZERO, output);
            freg(i->target, output);
            fwrite("0000000000000000000", 19, sizeof(char), output);
            break;
        case END:
            fwrite("111111", 6, sizeof(char), output);
            fwrite("00000000000000000000000000000000000", 35, sizeof(char), output);
            break;
        // Pseudo Instructions
        case LI:
            perror("li instr is not replaced");
            exit(1);
        case BLT:
            perror("blt instr is not replaced");
            exit(1);
        case MOVE:
            perror("move instr is not replaced");
            exit(1);
    }
    return;
}

void toBinary(FILE *output) {
    for (Bundle *b = bundles; b != NULL; b = b->next) {
        instructionToBinary(output, b->line, &(b->i0));
        fwrite("\n", 1, sizeof(char), output);
        instructionToBinary(output, b->line, &(b->i1));
        fwrite("\n", 1, sizeof(char), output);
        instructionToBinary(output, b->line, &(b->i2));
        fshamt(b->type, output);
        fwrite("\n", 1, sizeof(char), output);
    }
}

void toBinarySeparated(FILE *instr, FILE *instr0, FILE *instr1, FILE *instr2, FILE *instr3) {
    char *buffer = malloc(sizeof(char)*131);
    while (fread(buffer, 131, sizeof(char), instr)) {
        fwrite(buffer, 32, sizeof(char), instr0);
        fwrite("\n", 1, sizeof(char), instr0);
        fwrite(buffer+32, 9, sizeof(char), instr1);
        fwrite(buffer+42, 23, sizeof(char), instr1);
        fwrite("\n", 1, sizeof(char), instr1);
        fwrite(buffer+65, 18, sizeof(char), instr2);
        fwrite(buffer+84, 14, sizeof(char), instr2);
        fwrite("\n", 1, sizeof(char), instr2);
        fwrite(buffer+98, 32, sizeof(char), instr3);
        fwrite("\n", 1, sizeof(char), instr3);
    }
}

void instructionToDebug(FILE *output, Instruction *i) {
    switch (i->mnemo) {
        case NOP:
            fwrite("nop", 3, sizeof(char), output);
            break;
        // R-type
        case ADD:
            fwrite("add ", 4, sizeof(char), output);
            fregname(i->dist, output);
            fwrite(", ", 2, sizeof(char), output);
            fregname(i->source, output);
            fwrite(", ", 2, sizeof(char), output);
            fregname(i->target, output);
            break;
        case AND:
            fwrite("and ", 4, sizeof(char), output);
            fregname(i->dist, output);
            fwrite(", ", 2, sizeof(char), output);
            fregname(i->source, output);
            fwrite(", ", 2, sizeof(char), output);
            fregname(i->target, output);
            break;
        case JR:
            fwrite("jr ", 3, sizeof(char), output);
            fregname(i->source, output);
            break;
        case JALR:
            fwrite("jalr ", 5, sizeof(char), output);
            fregname(i->source, output);
            break;
        case OR:
            fwrite("or ", 3, sizeof(char), output);
            fregname(i->dist, output);
            fwrite(", ", 2, sizeof(char), output);
            fregname(i->source, output);
            fwrite(", ", 2, sizeof(char), output);
            fregname(i->target, output);
            break;
        case SLL:
            fwrite("sll ", 4, sizeof(char), output);
            fregname(i->dist, output);
            fwrite(", ", 2, sizeof(char), output);
            fregname(i->target, output);
            fwrite(", ", 2, sizeof(char), output);
            fprintf(output, "%d", i->immi);
            break;
        case SRL:
            fwrite("srl ", 4, sizeof(char), output);
            fregname(i->dist, output);
            fwrite(", ", 2, sizeof(char), output);
            fregname(i->target, output);
            fwrite(", ", 2, sizeof(char), output);
            fprintf(output, "%d", i->immi);
            break;
        case SLT:
            fwrite("slt ", 3, sizeof(char), output);
            fregname(i->dist, output);
            fwrite(", ", 2, sizeof(char), output);
            fregname(i->source, output);
            fwrite(", ", 2, sizeof(char), output);
            fregname(i->target, output);
            break;
        case SUB:
            fwrite("sub ", 4, sizeof(char), output);
            fregname(i->dist, output);
            fwrite(", ", 2, sizeof(char), output);
            fregname(i->source, output);
            fwrite(", ", 2, sizeof(char), output);
            fregname(i->target, output);
            break;
        // I-type
        case ADDI:
            fwrite("addi ", 5, sizeof(char), output);
            fregname(i->target, output);
            fwrite(", ", 2, sizeof(char), output);
            fregname(i->source, output);
            fwrite(", ", 2, sizeof(char), output);
            fprintf(output, "%d", i->immi);
            break;
        case ORI:
            fwrite("ori ", 4, sizeof(char), output);
            fregname(i->target, output);
            fwrite(", ", 2, sizeof(char), output);
            fregname(i->source, output);
            fwrite(", ", 2, sizeof(char), output);
            fprintf(output, "%d", i->immi);
            break;
        case SLTI:
            fwrite("slti ", 5, sizeof(char), output);
            fregname(i->target, output);
            fwrite(", ", 2, sizeof(char), output);
            fregname(i->source, output);
            fwrite(", ", 2, sizeof(char), output);
            fprintf(output, "%d", i->immi);
            break;
        case BEQ:
            fwrite("beq ", 4, sizeof(char), output);
            fregname(i->target, output);
            fwrite(", ", 2, sizeof(char), output);
            fregname(i->source, output);
            fwrite(", ", 2, sizeof(char), output);
            fprintf(output, "%s", i->label);
            break;
        case BNE:
            fwrite("bne ", 4, sizeof(char), output);
            fregname(i->target, output);
            fwrite(", ", 2, sizeof(char), output);
            fregname(i->source, output);
            fwrite(", ", 2, sizeof(char), output);
            fprintf(output, "%s", i->label);
            break;
        case LUI:
            fwrite("lui ", 4, sizeof(char), output);
            fregname(i->target, output);
            fwrite(", ", 2, sizeof(char), output);
            fprintf(output, "%d", i->immi);
            break;
        case LW:
            fwrite("lw ", 3, sizeof(char), output);
            fregname(i->target, output);
            fwrite(", ", 2, sizeof(char), output);
            fprintf(output, "%d", i->immi);
            fwrite("(", 1, sizeof(char), output);
            fregname(i->source, output);
            fwrite(")", 1, sizeof(char), output);
            break;
        case SW:
            fwrite("sw ", 3, sizeof(char), output);
            fregname(i->target, output);
            fwrite(", ", 2, sizeof(char), output);
            fprintf(output, "%d", i->immi);
            fwrite("(", 1, sizeof(char), output);
            fregname(i->source, output);
            fwrite(")", 1, sizeof(char), output);
            break;
        // J-type
        case J:
            fwrite("j ", 2, sizeof(char), output);
            fprintf(output, "%s", i->label);
            break;
        case JAL:
            fwrite("jal ", 4, sizeof(char), output);
            fprintf(output, "%s", i->label);
            break;
        // IO Instructions
        case IN:
            fwrite("in ", 3, sizeof(char), output);
            fregname(i->target, output);
            break;
        case OUT:
            fwrite("out ", 4, sizeof(char), output);
            fregname(i->target, output);
            break;
        // Float Instructions
        case BTS:
            fwrite("bts ", 4, sizeof(char), output);
            fprintf(output, "%d", i->immi);
            fwrite(", ", 2, sizeof(char), output);
            fprintf(output, "%s", i->label);
            break;
        case BFS:
            fwrite("bfs ", 4, sizeof(char), output);
            fprintf(output, "%d", i->immi);
            fwrite(", ", 2, sizeof(char), output);
            fprintf(output, "%s", i->label);
            break;
        case ADDS:
            fwrite("add.s ", 6, sizeof(char), output);
            ffregname(i->dist, output);
            fwrite(", ", 2, sizeof(char), output);
            ffregname(i->source, output);
            fwrite(", ", 2, sizeof(char), output);
            ffregname(i->target, output);
            break;
        case SUBS:
            fwrite("sub.s ", 6, sizeof(char), output);
            ffregname(i->dist, output);
            fwrite(", ", 2, sizeof(char), output);
            ffregname(i->source, output);
            fwrite(", ", 2, sizeof(char), output);
            ffregname(i->target, output);
            break;
        case MULS:
            fwrite("mul.s ", 6, sizeof(char), output);
            ffregname(i->dist, output);
            fwrite(", ", 2, sizeof(char), output);
            ffregname(i->source, output);
            fwrite(", ", 2, sizeof(char), output);
            ffregname(i->target, output);
            break;
        case DIVS:
            fwrite("div.s ", 6, sizeof(char), output);
            ffregname(i->dist, output);
            fwrite(", ", 2, sizeof(char), output);
            ffregname(i->source, output);
            fwrite(", ", 2, sizeof(char), output);
            ffregname(i->target, output);
            break;
        case MOVS:
            fwrite("mov.s ", 6, sizeof(char), output);
            ffregname(i->dist, output);
            fwrite(", ", 2, sizeof(char), output);
            ffregname(i->target, output);
            break;
        case NEGS:
            fwrite("neg.s ", 6, sizeof(char), output);
            ffregname(i->dist, output);
            fwrite(", ", 2, sizeof(char), output);
            ffregname(i->target, output);
            break;
        case ABSS:
            fwrite("abs.s ", 6, sizeof(char), output);
            ffregname(i->dist, output);
            fwrite(", ", 2, sizeof(char), output);
            ffregname(i->target, output);
            break;
        case SQRTS:
            fwrite("sqrt.s ", 7, sizeof(char), output);
            ffregname(i->dist, output);
            fwrite(", ", 2, sizeof(char), output);
            ffregname(i->target, output);
            break;
        case CEQS:
            fwrite("c.eq.s ", 6, sizeof(char), output);
            fprintf(output, "%d", i->immi);
            fwrite(", ", 2, sizeof(char), output);
            ffregname(i->source, output);
            fwrite(", ", 2, sizeof(char), output);
            ffregname(i->target, output);
            break;
        case CLTS:
            fwrite("c.lt.s ", 6, sizeof(char), output);
            fprintf(output, "%d", i->immi);
            fwrite(", ", 2, sizeof(char), output);
            ffregname(i->source, output);
            fwrite(", ", 2, sizeof(char), output);
            ffregname(i->target, output);
            break;
        case CLES:
            fwrite("c.le.s ", 6, sizeof(char), output);
            fprintf(output, "%d", i->immi);
            fwrite(", ", 2, sizeof(char), output);
            ffregname(i->source, output);
            fwrite(", ", 2, sizeof(char), output);
            ffregname(i->target, output);
            break;
        case LWS:
            fwrite("lw.s ", 5, sizeof(char), output);
            ffregname(i->target, output);
            fwrite(", ", 2, sizeof(char), output);
            fprintf(output, "%d", i->immi);
            fwrite("(", 1, sizeof(char), output);
            fregname(i->source, output);
            fwrite(")", 1, sizeof(char), output);
            break;
        case SWS:
            fwrite("sw.s ", 5, sizeof(char), output);
            ffregname(i->target, output);
            fwrite(", ", 2, sizeof(char), output);
            fprintf(output, "%d", i->immi);
            fwrite("(", 1, sizeof(char), output);
            fregname(i->source, output);
            fwrite(")", 1, sizeof(char), output);
            break;
        case FTOI:
            fwrite("ftoi ", 5, sizeof(char), output);
            fregname(i->target, output);
            fwrite(", ", 2, sizeof(char), output);
            ffregname(i->source, output);
            break;
        case ITOF:
            fwrite("itof ", 5, sizeof(char), output);
            ffregname(i->target, output);
            fwrite(", ", 2, sizeof(char), output);
            fregname(i->source, output);
            break;
        case CADD:
            fwrite("cadd ", 5, sizeof(char), output);
            ffregname(i->target, output);
            fwrite(", ", 2, sizeof(char), output);
            fregname(i->source, output);
            break;
        case FORK:
            fwrite("fork ", 5, sizeof(char), output);
            fregname(i->source, output);
            fwrite(", ", 2, sizeof(char), output);
            fprintf(output, "%d", i->immi);
            break;
        case NEXT:
            fwrite("next ", 5, sizeof(char), output);
            fregname(i->target, output);
            break;
        case END:
            fwrite("end", 3, sizeof(char), output);
            break;
        // Pseudo Instructions
        case LI:
            perror("li instr is not replaced");
            exit(1);
        case BLT:
            perror("blt instr is not replaced");
            exit(1);
        case MOVE:
            perror("move instr is not replaced");
            exit(1);
    }
}
void toDebug(FILE *output) {
    int label_maxlen = 0;
    for (Label *l = labels; l != NULL; l = l->next)
        if (strlen(l->name) > label_maxlen)
            label_maxlen = strlen(l->name);

    for (Bundle *b = bundles; b != NULL; b = b->next) {
        fline(b->line, output); flabel(b->line, label_maxlen, output); instructionToDebug(output, &(b->i0));
        fwrite("\n", 1, sizeof(char), output);
        fline(b->line, output); fnonlabel(b->line, label_maxlen, output); instructionToDebug(output, &(b->i1));
        fwrite("\n", 1, sizeof(char), output);
        fline(b->line, output); fnonlabel(b->line, label_maxlen, output); instructionToDebug(output, &(b->i2));
        fprintf(output, " (%d)", b->type);
        fwrite("\n", 1, sizeof(char), output);
    }
    return;
}

// for debug
void printMnemonic(enum mnemonic mnemo) {
    switch (mnemo) {
        case NOP: printf("nop"); break;
        case ADD: printf("add"); break;
        case AND: printf("and"); break;
        case JR: printf("jr"); break;
        case JALR: printf("jalr"); break;
        case OR: printf("or"); break;
        case SLL: printf("sll"); break;
        case SRL: printf("srl"); break;
        case SLT: printf("slt"); break;
        case SUB: printf("sub"); break;
        case ADDI: printf("addi"); break;
        case SLTI: printf("slti"); break;
        case BEQ: printf("beq"); break;
        case BNE: printf("bne"); break;
        case LUI: printf("lui"); break;
        case LW: printf("lw"); break;
        case ORI: printf("ori"); break;
        case SW: printf("sw"); break;
        case J: printf("j"); break;
        case JAL: printf("jal"); break;
        case IN: printf("in"); break;
        case OUT: printf("out"); break;
        case BTS: printf("bt.s"); break;
        case BFS: printf("bf.s"); break;
        case ADDS: printf("add.s"); break;
        case SUBS: printf("sub.s"); break;
        case MULS: printf("mul.s"); break;
        case DIVS: printf("div.s"); break;
        case MOVS: printf("mov.s"); break;
        case NEGS: printf("neg.s"); break;
        case ABSS: printf("abs.s"); break;
        case SQRTS: printf("sqrt.s"); break;
        case CEQS: printf("c.eq.s"); break;
        case CLTS: printf("c.lt.s"); break;
        case CLES: printf("c.le.s"); break;
        case LWS: printf("lw.s"); break;
        case SWS: printf("sw.s"); break;
        case FTOI: printf("ftoi.s"); break;
        case ITOF: printf("itof.s"); break;
        case CADD: printf("cadd"); break;
        case FORK: printf("fork"); break;
        case NEXT: printf("next"); break;
        case END: printf("end"); break;
        case LI: printf("li"); break;
        case BLT: printf("blt"); break;
        case MOVE: printf("move"); break; 
    } 
}

// for debug
void printReg(enum reg reg) {
    switch (reg) {
        case ZERO: printf("$zero"); break;
        case V0: printf("$v0"); break;
        case V1: printf("$v1"); break;
        case AT: printf("$at"); break;
        case A0: printf("$a0"); break;
        case A1: printf("$a1"); break;
        case A2: printf("$a2"); break;
        case A3: printf("$a3"); break;
        case T0: printf("$t0"); break;
        case T1: printf("$t1"); break;
        case T2: printf("$t2"); break;
        case T3: printf("$t3"); break;
        case T4: printf("$t4"); break;
        case T5: printf("$t5"); break;
        case T6: printf("$t6"); break;
        case T7: printf("$t7"); break;
        case S0: printf("$s0"); break;
        case S1: printf("$s1"); break;
        case S2: printf("$s2"); break;
        case S3: printf("$s3"); break;
        case S4: printf("$s4"); break;
        case S5: printf("$s5"); break;
        case S6: printf("$s6"); break;
        case S7: printf("$s7"); break;
        case T8: printf("$t8"); break;
        case T9: printf("$t9"); break;
        case K0: printf("$k0"); break;
        case K1: printf("$k1"); break;
        case GP: printf("$gp"); break;
        case SP: printf("$sp"); break;
        case FP: printf("$fp"); break;
        case RA: printf("$ra"); break;
        case None: printf("None"); break;
    }
}

// for debug
void printInstructions(void) {
    // for (Instruction *i = instrs; i != NULL; i = i->next) {
    //     printMnemonic(i->mnemo); printf("\n");
    //     printf("    line: %d\n", i->line);
    //     printf("    source: "); printReg(i->source); printf("\n");
    //     printf("    target: "); printReg(i->target); printf("\n");
    //     printf("    dist: "); printReg(i->dist); printf("\n");
    //     printf("    immi: %d\n", i->immi);
    //     printf("    label: %s\n", i->label);
    //     printf("    label_line: %d\n", i->label_line);
    // }
    return;
}
