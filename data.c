#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "data.h"

void appendDatum(int l, int n, int d) {
    Data *new_datum = malloc(sizeof(Data));
    new_datum->line = l;
    new_datum->num = n;
    new_datum->datum = d;
    new_datum->next = NULL;

    if (data == NULL) data = new_datum;
    else {
        Data *datum = data;
        for (; datum->next != NULL; datum = datum->next);
        datum->next = new_datum;
    }
}

int lookupData(int l) {
    for (Data *d = data; d != NULL; d = d->next) {
        if (d->line == l) return d->line;
    }

    perror("Data Not Found");
    exit(1);
}


char *datumtobinary(int d) {
    char *datum = malloc(sizeof(char)*32);
    for (int i=0; i<32; i++) {
        if (d>>(31-i) & 1) datum[i] = '1';
        else datum[i] = '0';
    }
    return datum;
}

void toData(FILE *data_f) {
    for (Data *d = data; d != NULL; d = d->next)
        for (int i=0; i < d->num; i++) {
        fwrite(datumtobinary(d->datum), 32, sizeof(char), data_f);
        fwrite("\n", 1, sizeof(char), data_f);
        }
}

// for debug
void printData(void) {
    printf("data\n");
    for (Data *d = data; d != NULL; d = d->next) {
        printf("    datum: %d\n", d->datum);
    }
}
