CC = gcc

all: assembler

assembler: lex.yy.c y.tab.c main.c
	$(CC) main.c lex.yy.c y.tab.c instruction.c label.c data.c -o $@

lex.yy.c: lexer.l
	lex lexer.l

y.tab.c: yacc.y
	yacc --debug --verbose -d yacc.y

clean:
	rm -f *.o lex.yy.c y.tab.* assembler y.output
