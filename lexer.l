%option stack

%{
#include <stdio.h>
#include "y.tab.h"
%}

%%
\#(.*)                  {};    

".globl"                return GLOBL;
".data"                 return DATA;
".text"                 return TEXT;

"nop"                   return NOP_M;
"add"                   return ADD_M;
"addi"                  return ADDI_M;
"sub"                   return SUB_M;
"and"                   return AND_M;
"beq"                   return BEQ_M;
"bne"                   return BNE_M;
"j"                     return J_M;
"jal"                   return JAL_M;
"jr"                    return JR_M;
"jalr"                  return JALR_M;
"lw"                    return LW_M;
"lui"                   return LUI_M;
"or"                    return OR_M;
"ori"                   return ORI_M;
"sll"                   return SLL_M;
"srl"                   return SRL_M;
"slt"                   return SLT_M;
"slti"                  return SLTI_M;
"sw"                    return SW_M;
"in"                    return IN_M;
"out"                   return OUT_M;
"bt.s"                  return BTS_M;
"bf.s"                  return BFS_M;
"add.s"                 return ADDS_M;
"sub.s"                 return SUBS_M;
"mul.s"                 return MULS_M;
"div.s"                 return DIVS_M;
"mov.s"                 return MOVS_M;
"neg.s"                 return NEGS_M;
"abs.s"                 return ABSS_M;
"sqrt.s"                return SQRTS_M;
"c.eq.s"                return CEQS_M;
"c.lt.s"                return CLTS_M;
"c.le.s"                return CLES_M;
"lw.s"                  return LWS_M;
"sw.s"                  return SWS_M;
"ftoi"                  return FTOI_M;
"itof"                  return ITOF_M;
"cadd"                  return CADD_M;
"fork"                  return FORK_M;
"next"                  return NEXT_M;
"end"                   return END_M;
"li"                    return LI_M;
"blt"                   return BLT_M;
"move"                  return MOVE_M;
"la"                    return LA_M;

"$zero"                 return ZERO_R;
"$zr"                   return ZERO_R;
"$v0"                   return V0_R;
"$v1"                   return V1_R;
"$at"                   return AT_R;
"$a0"                   return A0_R;
"$a1"                   return A1_R;
"$a2"                   return A2_R;
"$a3"                   return A3_R;
"$t0"                   return T0_R;
"$t1"                   return T1_R;
"$t2"                   return T2_R;
"$t3"                   return T3_R;
"$t4"                   return T4_R;
"$t5"                   return T5_R;
"$t6"                   return T6_R;
"$t7"                   return T7_R;
"$s0"                   return S0_R;
"$s1"                   return S1_R;
"$s2"                   return S2_R;
"$s3"                   return S3_R;
"$s4"                   return S4_R;
"$s5"                   return S5_R;
"$s6"                   return S6_R;
"$s7"                   return S7_R;
"$t8"                   return T8_R;
"$t9"                   return T9_R;
"$k0"                   return K0_R;
"$k1"                   return K1_R;
"$gp"                   return GP_R;
"$sp"                   return SP_R;
"$fp"                   return FP_R;
"$ra"                   return RA_R;

"$f0"                   return F0_R;
"$f1"                   return F1_R;
"$f2"                   return F2_R;
"$f3"                   return F3_R;
"$f4"                   return F4_R;
"$f5"                   return F5_R;
"$f6"                   return F6_R;
"$f7"                   return F7_R;
"$f8"                   return F8_R;
"$f9"                   return F9_R;
"$f10"                  return F10_R;
"$f11"                  return F11_R;
"$f12"                  return F12_R;
"$f13"                  return F13_R;
"$f14"                  return F14_R;
"$f15"                  return F15_R;
"$f16"                  return F16_R;
"$f17"                  return F17_R;
"$f18"                  return F18_R;
"$f19"                  return F19_R;
"$f20"                  return F20_R;
"$f21"                  return F21_R;
"$f22"                  return F22_R;
"$f23"                  return F23_R;
"$f24"                  return F24_R;
"$f25"                  return F25_R;
"$f26"                  return F26_R;
"$f27"                  return F27_R;
"$f28"                  return F28_R;
"$f29"                  return F29_R;
"$f30"                  return F30_R;
"$f31"                  return F31_R;

":"                     return COLON;
","                     return COMMA;
"("                     return LParenthesis;
")"                     return RParenthesis;

[a-zA-Z_][a-zA-Z0-9_.]*  {
    yylval.name = strdup(yytext);
    return LABEL;
}

0x[0-9a-f]+ {
    yylval.fl = strdup(yytext);
    return FLOAT;
}

-?[0-9]+           {
    int tmp;
    sscanf(yytext, "%d", &tmp);
    yylval.immi = tmp;
    return IMMI;
}

[ \n]                    {}
"."                     {}

%%
