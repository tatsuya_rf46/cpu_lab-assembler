typedef struct label {
    char *name;
    int line;
    struct label *next;
} Label;

// Global Variable
Label *labels;
Label *dlabels;

void appendLabel(char *name, int l);
void appendDLabel(char *name, int l);
int lookupLabel(char *name);
void printLabels(void);
