%{
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "data.h"
#include "label.h"
#include "instruction.h"

#define YYMAXDEPTH 1000000
#define YYINITDEPTH 100000

void yyerror(const char *str)
{
    fprintf(stderr,"line: %d\n", line);
    fprintf(stderr,"error: %s\n", str);
}

int yywrap()
{
    return 1;
} 

// nop instruction
struct instruction nop = {
    .mnemo = NOP,
    .source = None,
    .target = None,
    .dist = None,
    .immi = 0xfffffff,
    .label = NULL,
    .label_line = -1
    };

int yylex();
%}

%union {
    char *name;
    char *fl;
    int reg_num;
    int immi;
    int mnemo;
    struct instruction *instr;
}

%token GLOBL DATA TEXT
%token NOP_M
%token ADD_M SUB_M AND_M JR_M JALR_M OR_M SLL_M SRL_M SLT_M SLTI_M
%token ADDI_M ORI_M BEQ_M BNE_M LW_M LUI_M SW_M
%token J_M JAL_M
%token IN_M OUT_M
%token BTS_M BFS_M
%token ADDS_M SUBS_M MULS_M DIVS_M MOVS_M NEGS_M ABSS_M SQRTS_M
%token CEQS_M CLTS_M CLES_M
%token LWS_M SWS_M
%token FTOI_M ITOF_M
%token CADD_M
%token FORK_M NEXT_M END_M
%token LI_M BLT_M MOVE_M LA_M
%token <reg_num> ZERO_R
%token <reg_num> V0_R V1_R
%token <reg_num> AT_R
%token <reg_num> A0_R A1_R A2_R A3_R
%token <reg_num> T0_R T1_R T2_R T3_R T4_R T5_R T6_R T7_R
%token <reg_num> S0_R S1_R S2_R S3_R S4_R S5_R S6_R S7_R
%token <reg_num> T8_R T9_R
%token <reg_num> K0_R K1_R
%token <reg_num> GP_R
%token <reg_num> SP_R
%token <reg_num> FP_R
%token <reg_num> RA_R
%token <reg_num> F0_R F1_R F2_R F3_R F4_R F5_R F6_R F7_R
%token <reg_num> F8_R F9_R F10_R F11_R F12_R F13_R F14_R F15_R
%token <reg_num> F16_R F17_R F18_R F19_R F20_R F21_R F22_R F23_R
%token <reg_num> F24_R F25_R F26_R F27_R F28_R F29_R F30_R F31_R
%token COLON COMMA LParenthesis RParenthesis
%token <name> LABEL
%token <immi> IMMI
%token <fl> FLOAT

%type <name> label
%type <reg_num> reg
%type <mnemo> reg_reg_type reg_reg_reg_type i_type j_type cond_type cond_branch_type
%type <instr> instr

%start top

%%
top:
        data
        texts
        {
            /*
                stub
            */
            // top label
            appendLabel("top", line);

            appendBundle(
                line,
                *returnInstruction(LI, None, None, AT, 0xaa, NULL),
                nop,
                nop,
                0
            );
            line += 4;
            appendBundle(
                line,
                *returnInstruction(OUT, None, AT, None, 0xfffffff, NULL),
                nop,
                nop,
                0
            );
            line += 4;

            // push GP as much as Data Space
            // push SP as much as Data Space and Heap Space
            appendBundle(
                line,
                *returnInstruction(ADDI, GP, GP, None, data_line, NULL),
                *returnInstruction(LUI, None, SP, None, 8, NULL),
                nop,
                0
            );
            line += 4;
            // Jump to globl
            appendBundle(
                line,
                *returnInstruction(SW, SP, RA, None, 0, NULL),
                nop,
                nop,
                0
            );
            line += 4;
            appendBundle(
                line,
                *returnInstruction(ADDI, SP, SP, None, 4, NULL),
                nop,
                nop,
                0
            );
            line += 4;
            appendBundle(
                line,
                nop,
                nop,
                *returnInstruction(JAL, None, None, None, 0xfffffff, globl),
                4
            );
            line += 4;
            appendBundle(
                line,
                *returnInstruction(ADDI, SP, SP, None, -4, NULL),
                nop,
                nop,
                0
            );
            line += 4;
            appendBundle(
                line,
                *returnInstruction(LW, SP, RA, None, 0, NULL),
                nop,
                nop,
                0
            );
            line += 4;
            // Infinite loop at the last
            appendLabel("loop", line);
            appendBundle(
                line,
                nop,
                nop,
                *returnInstruction(J, None, None, None, 0xfffffff, "loop"),
                4
            );
            line += 4;
        }
        ;

data:  /* empty */
        |
        datum
        data
        ;

datum:  /* empty */
        LABEL COLON
        {
            appendDLabel($1, data_line);
        }
        |
        FLOAT
        {
            // Given FLOAT is Hex
            appendDatum(data_line, 1, strtol($1, NULL, 0));
            data_line += 4;
        }
        |
        IMMI COMMA FLOAT
        {
            appendDatum(data_line, $1, strtol($3, NULL, 0));
            data_line += 4 * $1;
        }
        |
        IMMI COMMA LABEL
        {
            appendDatum(data_line, $1, lookupLabel($3));
            data_line += 4 * $1;
        }
        ;

texts:
        TEXT
        GLOBL LABEL
        {
            // Jump to Start function
            setGlobal($3);
            appendBundle(
                line,
                nop,
                nop,
                *returnInstruction(J, None, None, None, 0xfffffff, "top"),
                4
            );
            line += 4;
        }
        text
        ;

text:  /* empty */
        |
        code text
        ;

code:
        label
        |
        bundle
        ;

label:
        LABEL COLON
        {
            appendLabel($1, line);
        }
        ;

bundle:
        instr instr instr IMMI
        {
            appendBundle(line, *$1, *$2, *$3, $4);
            line += 4;
        }

instr:
        NOP_M
        {
            $$ = &nop;
        }
        |
        END_M
        {
            $$ = returnInstruction(END, None, None, None, 0xfffffff, NULL);
        }
        |
        NEXT_M reg
        {
            $$ = returnInstruction(NEXT, None, $2, None, 0xfffffff, NULL);
        }
        |
        FORK_M reg COMMA IMMI
        {
            $$ = returnInstruction(FORK, $2, None, None, $4, NULL);
        }
        |
        SLL_M reg COMMA reg COMMA IMMI
        {
            $$ = returnInstruction(SLL, None, $4, $2, $6, NULL);
        }
        |
        SRL_M reg COMMA reg COMMA IMMI
        {
            $$ = returnInstruction(SRL, None, $4, $2, $6, NULL);
        }
        |
        JR_M reg
        {
            $$ = returnInstruction(JR, $2, None, None, 0xfffffff, NULL);
        }
        |
        JALR_M reg
        {
            $$ = returnInstruction(JALR, $2, None, None, 0xfffffff, NULL);
        }
        |
        BEQ_M reg COMMA reg COMMA LABEL
        {
            $$ = returnInstruction(BEQ, $2, $4, None, 0xfffffff, $6);
        }
        |
        BNE_M reg COMMA reg COMMA LABEL
        {
            $$ = returnInstruction(BNE, $2, $4, None, 0xfffffff, $6);
        }
        |
        LW_M reg COMMA LABEL
        {
            $$ = returnInstruction(LW, ZERO, $2, None, lookupData(lookupLabel($4)), NULL);
        }
        |
        LW_M reg COMMA IMMI LParenthesis reg RParenthesis
        {
            $$ = returnInstruction(LW, $6, $2, None, $4, NULL);
        }
        |
        LUI_M reg COMMA IMMI
        {
            $$ = returnInstruction(LUI, None, $2, None, $4, NULL);
        }
        |
        SW_M reg COMMA IMMI LParenthesis reg RParenthesis
        {
            $$ = returnInstruction(SW, $6, $2, None, $4, NULL);
        }
        |
        IN_M reg
        {
            $$ = returnInstruction(IN, None, $2, None, 0xfffffff, NULL);
        }
        |
        OUT_M reg
        {
            $$ = returnInstruction(OUT, None, $2, None, 0xfffffff, NULL);
        }
        |
        FTOI_M reg COMMA reg
        {
            $$ = returnInstruction(FTOI, $4, $2, None, 0xfffffff, NULL);
        }
        |
        ITOF_M reg COMMA reg
        {
            $$ = returnInstruction(ITOF, $4, $2, None, 0xfffffff, NULL);
        }
        |
        CADD_M reg COMMA reg
        {
            $$ = returnInstruction(CADD, $4, $2, None, 0xfffffff, NULL);
        }
        |
        LWS_M reg COMMA LABEL
        {
            $$ = returnInstruction(LWS, ZERO, $2, None, lookupData(lookupLabel($4)), NULL);
        }
        |
        LWS_M reg COMMA IMMI LParenthesis reg RParenthesis
        {
            $$ = returnInstruction(LWS, $6, $2, None, $4, NULL);
        }
        |
        SWS_M reg COMMA IMMI LParenthesis reg RParenthesis
        {
            $$ = returnInstruction(SWS, $6, $2, None, $4, NULL);
        }
        |
        MOVE_M reg COMMA reg
        {
            $$ = returnInstruction(MOVE, $4, None, $2, 0xfffffff, NULL);
        }
        |
        BLT_M reg COMMA reg COMMA LABEL
        {
            $$ = returnInstruction(BLT, $2, $4, None, 0xfffffff, $6);
        }
        |
        LI_M reg COMMA IMMI
        {
            $$ = returnInstruction(LI, None, None, $2, $4, NULL);
        }
        |
        LA_M reg COMMA LABEL
        {
            $$ = returnInstruction(LI, None, None, $2, lookupLabel($4), NULL);
        }
        |
        reg_reg_reg_type reg COMMA reg COMMA reg
        {
            $$ = returnInstruction($1, $4, $6, $2, 0xfffffff, NULL);
        }
        |
        j_type LABEL
        {
            $$ = returnInstruction($1, None, None, None, 0xfffffff, $2);
        }
        |
        i_type reg COMMA reg COMMA IMMI
        {
            $$ = returnInstruction($1, $4, $2, None, $6, NULL);
        }
        |
        reg_reg_type reg COMMA reg
        {
            $$ = returnInstruction($1, None, $4, $2, 0xfffffff, NULL);
        }
        |
        cond_type IMMI COMMA reg COMMA reg
        {
            $$ = returnInstruction($1, $4, $6, None, $2, NULL);
        }
        |
        cond_branch_type IMMI COMMA LABEL
        {
            // Warning: The condition code is substituted as reg.
            $$ = returnInstruction($1, None, None, None, $2, $4);
        }
        ;

reg_reg_reg_type:
        ADD_M {$$ = ADD;}| SUB_M {$$ = SUB;}| AND_M {$$ = AND;}| OR_M {$$ = OR;}| SLT_M {$$ = SLT;}|
        ADDS_M {$$ = ADDS;}| SUBS_M {$$ = SUBS;}| MULS_M {$$ = MULS;}| DIVS_M {$$ = DIVS;}
        ;

i_type:
        ADDI_M {$$ = ADDI;}| ORI_M {$$ = ORI;}| SLTI_M {$$ = SLTI;}
        ;

j_type:
        J_M {$$ = J;} | JAL_M {$$ = JAL;}
        ;

reg_reg_type:
        MOVS_M {$$ = MOVS;}| NEGS_M {$$ = NEGS;}| ABSS_M {$$ = ABSS;}| SQRTS_M {$$ = SQRTS;}
        ;

cond_type:
        CEQS_M {$$ = CEQS;}| CLTS_M {$$ = CLTS;}| CLES_M {$$ = CLES;}
        ;

cond_branch_type:
        BTS_M {$$ = BTS;}| BFS_M {$$ = BFS;}
        ;

reg:
        ZERO_R {$$ = 0;} |
        AT_R {$$ = 1;} |
        V0_R {$$ = 2;} | V1_R {$$ = 3;} |
        A0_R {$$ = 4;} | A1_R {$$ = 5;} | A2_R {$$ = 6;} | A3_R {$$ = 7;} |
        T0_R {$$ = 8;} | T1_R {$$ = 9;} | T2_R {$$ = 10;} | T3_R {$$ = 11;} | T4_R {$$ = 12;} | T5_R {$$ = 13;} | T6_R {$$ = 14;} | T7_R {$$ = 15;} |
        S0_R {$$ = 16;} | S1_R {$$ = 17;} | S2_R {$$ = 18;} | S3_R {$$ = 19;} | S4_R {$$ = 20;} | S5_R {$$ = 21;} | S6_R {$$ = 22;} | S7_R {$$ = 23;} |
        T8_R {$$ = 24;} | T9_R {$$ = 25;} |
        K0_R {$$ = 26;} | K1_R {$$ = 27;} |
        GP_R {$$ = 28;} |
        SP_R {$$ = 29;} |
        FP_R {$$ = 30;} |
        RA_R {$$ = 31;} |
        F0_R {$$ = 0;} | F1_R {$$ = 1;} | F2_R {$$ = 2;} | F3_R {$$ = 3;} | F4_R {$$ = 4;} | F5_R {$$ = 5;} | 
        F6_R {$$ = 6;} | F7_R {$$ = 7;} | F8_R {$$ = 8;} | F9_R {$$ = 9;} | F10_R {$$ = 10;} | F11_R {$$ = 11;} |
        F12_R {$$ = 12;} | F13_R {$$ = 13;} | F14_R {$$ = 14;} | F15_R {$$ = 15;} | F16_R {$$ = 16;} | F17_R {$$ = 17;} |
        F18_R {$$ = 18;} | F19_R {$$ = 19;} | F20_R {$$ = 20;} | F21_R {$$ = 21;} | F22_R {$$ = 22;} | F23_R {$$ = 23;} |
        F24_R {$$ = 24;} | F25_R {$$ = 25;} | F26_R {$$ = 26;} | F27_R {$$ = 27;} | F28_R {$$ = 28;} | F29_R {$$ = 29;} |
        F30_R {$$ = 30;} | F31_R {$$ = 31;}
        ;
%%
