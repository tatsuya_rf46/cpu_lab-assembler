#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "y.tab.h"
#include "data.h"
#include "label.h"
#include "instruction.h"

int main(int argc, char* argv[]) {

    // Argument Error Handling
    if(argc != 2) {
        perror("Argument Error\n    $./main input\nThis means the input file is 'input.s' and output files are 'input_instr.mem', 'input_data.mem', and 'input_debug.mem'\n");
        exit(1);
    }

    char input[strlen(argv[1]) + 2];
    char instr[strlen(argv[1]) + 10];
    char instr0[strlen(argv[1]) + 10];
    char instr1[strlen(argv[1]) + 10];
    char instr2[strlen(argv[1]) + 10];
    char instr3[strlen(argv[1]) + 10];
    char data[strlen(argv[1]) + 9];
    char debug[strlen(argv[1]) + 10];
    strcpy(input, argv[1]);
    strcpy(instr, argv[1]);
    strcpy(instr0, argv[1]);
    strcpy(instr1, argv[1]);
    strcpy(instr2, argv[1]);
    strcpy(instr3, argv[1]);
    strcpy(data, argv[1]);
    strcpy(debug, argv[1]);

    // Specifying the input file for Lex
    extern FILE *yyin;
    yyin = fopen(strcat(input, ".s"), "r");

    // The binary file
    FILE *instr_fd;
    instr_fd = fopen(strcat(instr, "_instr.mem"), "w");
    FILE *instr0_fd;
    instr0_fd = fopen(strcat(instr0, "_instr0.mem"), "w");
    FILE *instr1_fd;
    instr1_fd = fopen(strcat(instr1, "_instr1.mem"), "w");
    FILE *instr2_fd;
    instr2_fd = fopen(strcat(instr2, "_instr2.mem"), "w");
    FILE *instr3_fd;
    instr3_fd = fopen(strcat(instr3, "_instr3.mem"), "w");

    // The data file
    FILE *data_fd;
    data_fd = fopen(strcat(data, "_data.mem"), "w");

    // The debug file
    FILE *debug_fd;
    debug_fd = fopen(strcat(debug, "_debug.txt"), "w");

    // Initialization
    labels = NULL;
    // instrs = NULL;
    bundles = NULL;

    // Tokenizing and parsing
    yyparse();

    // printf("\n----- default -----\n");
    // printLabels();
    // printInstructions();

    // Replace pseudo-instructions
    replacePseudoinstructions();

    // printf("\n----- replaced -----\n");
    // printLabels();
    // printf("\n");
    // printInstructions();

    // Attach Label with its line number
    attachLabelLine();

    // printf("\n----- label replaced -----\n");
    // printLabels();
    // printInstructions();

    // Output the binary
    toBinary(instr_fd);
    fclose(instr_fd);
    instr_fd = fopen(instr, "r");
    toBinarySeparated(instr_fd, instr0_fd, instr1_fd, instr2_fd, instr3_fd);

    // Output the data
    toData(data_fd);

    // Output the debug file
    toDebug(debug_fd);

    // printf("\n");
    // printData();

    return 0;
}
