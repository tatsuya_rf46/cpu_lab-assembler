Cpu_lab-assembler
=================
Assembler that assembles instrutions of MIPS-subsets.

How to use
----------
Prerequisites: 'lex' and 'yacc'  
Ubuntu ->
```
sudo apt install flex bison 
```
  
Build 
```
make
```

Use
Input File: foo.s
Output Files:
- Instruction File: foo_instr.s
- Data File: foo_data.s
- Debug File: foo_debug.s
```
./main foo
```

Supported Syntax
----------------
Data spaces, Global declaration, labeling, and a few instructions.
```
    .data
label:
    data...

    .text
    .globl label
label:
    instructions...
```

Supported instructions  
R-format
- add, sub, and, jr, or, slt, 

I-format
- addi, ori, slti, beq, bne, lui, lw, sw  

J-format
- j, jal  

IO
- in, out

Float
- bt.s, bf.s, add.s, sub.s, mul.s, div.s, mov.s, neg.s, abs.s, sqrt.s, c.[eq|lt|le].s

Float-Int
- lw.s, sw.s, ftoi, itof

Pseudo-instructions
- li, blt, move

Assembling
----------
Assembler adds Jump instrucition to the label "top".  
In "top" label, this assembler adds $sp with data lines, and adds it with 4 for $ra.  
It also adds Jump instruction at the last which jumps to itself (infinite loop).

TODO
----
