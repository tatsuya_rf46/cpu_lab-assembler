#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "data.h"
#include "label.h"

void appendLabel(char *name, int l) {
    Label *new_label = malloc(sizeof(Label));
    new_label->name = name;
    new_label->line = l;
    new_label->next = NULL;

    if (labels == NULL) labels = new_label;
    else {
        Label *l = labels;
        for (; l->next != NULL; l = l->next);
        l->next = new_label;
    }
}

void appendDLabel(char *name, int l) {
    Label *new_label = malloc(sizeof(Label));
    new_label->name = name;
    new_label->line = l;
    new_label->next = NULL;

    if (dlabels == NULL) dlabels = new_label;
    else {
        Label *l = dlabels;
        for (; l->next != NULL; l = l->next);
        l->next = new_label;
    }
}

int lookupLabel(char *name) {
    // look up data label
    for (Label *d = dlabels; d != NULL; d = d->next) {
        if (strcmp(d->name, name) == 0) return d->line;
    }

    // look up function label
    for (Label *l = labels; l != NULL; l = l->next) {
        if (strcmp(l->name, name) == 0) return l->line;
    }

    fprintf(stderr, "%s\n", name);
    perror("Label Not Found");
    exit(1);
}

// for debug
void printLabels(void) {
    printf("labels\n");
    for (Label *l = labels; l != NULL; l = l->next) {
        printf("%s:\n", l->name);
        printf("    line: %d\n", l->line);
    }
}
