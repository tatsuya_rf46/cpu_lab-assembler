#include <stdio.h>

enum mnemonic {
    NOP,
    ADD, SUB, AND, JR, JALR, OR, SLT,
    ADDI, ORI, SLL, SRL, SLTI, BEQ, BNE, LUI, LW, SW,
    J, JAL,
    IN, OUT,
    BTS, BFS,
    ADDS, SUBS, MULS, DIVS,
    MOVS, NEGS, ABSS, SQRTS,
    CEQS, CLTS, CLES,
    LWS, SWS,
    FTOI, ITOF,
    CADD,
    FORK, NEXT, END,
    LI, BLT, MOVE
};

enum reg {
    ZERO,
    AT,
    V0, V1,
    A0, A1, A2, A3,
    T0, T1, T2, T3, T4, T5, T6, T7,
    S0, S1, S2, S3, S4, S5, S6, S7,
    T8, T9,
    K0, K1,
    GP,
    SP,
    FP,
    RA,
    None
};

// Nan values:
//     reg -> None
//     immi -> 0xfffffff (> 2^26)
//     *label -> NULL
//     label_line -> -1 (< 0)
//     *next -> NULL
typedef struct instruction {
    /* int line; */
    enum mnemonic mnemo;
    enum reg source;
    enum reg target;
    enum reg dist;
    int immi;
    char *label;
    int label_line;
    /* struct instruction *next; */
} Instruction;

typedef struct bundle {
    int line;
    Instruction i0;
    Instruction i1;
    Instruction i2;
    int type;
    struct bundle *next;
} Bundle;

// Global Variables
char *globl;
int line;
/* Instruction *instrs; */
Bundle *bundles;

void setGlobal(char *g);
Instruction *returnInstruction(
        enum mnemonic mnemo,
        enum reg source,
        enum reg target,
        enum reg dist,
        int immi,
        char *label
        );
/* void appendInstruction( */
/*         int line, */
/*         enum mnemonic mnemo, */
/*         enum reg source, */
/*         enum reg target, */
/*         enum reg dist, */
/*         int immi, */
/*         char *label */
/*         ); */
void appendBundle(
        int line,
        Instruction i0,
        Instruction i1,
        Instruction i2,
        int type
        );
void replacePseudoinstructions(void);
void attachLabelLine(void);
void toBinary(FILE *output);
void toBinarySeparated(FILE *instr, FILE *instr0, FILE *instr1, FILE *instr2, FILE *instr3);
void toDebug(FILE *output);
void printInstructions(void);
